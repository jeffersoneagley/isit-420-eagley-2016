﻿using MVCdiceGame.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCdiceGame.Controllers
{
    public class HomeController : Controller
    {
        private LogEntityModelContainer db = new LogEntityModelContainer();

        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        // POST: Home/Play
        [HttpPost]
        public ActionResult Play()
        {
            ChargeLog aChargeLog = new ChargeLog();
            decimal amount = Convert.ToDecimal(Request["amount"].ToString());
            aChargeLog.Amount = amount;
            aChargeLog.FirstName = Request["TextFirstName"].ToString();
            aChargeLog.LastName = Request["TextLastName"].ToString();
            aChargeLog.CreditCardNumber = Request["CreditCard"].ToString();
            ViewBag.chargeAmmount = amount;
            db.ChargeLogs.Add(aChargeLog);
            db.SaveChanges();
           
            return View();
        }

        // GET: Course
        public ActionResult ShowLog()
        {
            List<ChargeLog> logList = db.ChargeLogs.OrderBy(q => q.CreditCardNumber).ToList();
            return View(logList);
        }

         // GET: Course
        public ActionResult QueryLog()
        {
            LogEntityModelContainer myData = new LogEntityModelContainer();
            var queryResult =
                from record in myData.ChargeLogs
                where record.Amount > 100
                orderby record.CreditCardNumber
                select record;
            List <ChargeLog> logList =  queryResult.ToList();
            return View(logList);
        }
    }
}