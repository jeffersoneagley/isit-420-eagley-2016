﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AnalysisServices.AdomdClient;

namespace advWrksFormsMDX
{
    class MDXaccess
    {
        public DataSet GetData()
        {
            string commandText = " SELECT NONEMPTY ( [Dim Sales Territory].[Sales Territory Country].Children ) ON ROWS,"+
                " {[Measures].[Sales Amount], [Measures].[Order quantity]} ON COLUMNS " +
                " FROM [Adventure Works DW] ";

            DataSet cubeDataSet = new DataSet();

            AdomdConnection ADOconnection = new AdomdConnection("Data Source=localhost; Catalog=FirstBICube");
            AdomdCommand ADOcommand = new AdomdCommand(commandText, ADOconnection);
            AdomdDataAdapter myDataAdapter = new AdomdDataAdapter();
            myDataAdapter.SelectCommand = ADOcommand;

            try
            {
                ADOconnection.Open();
                myDataAdapter.Fill(cubeDataSet, "CubeData");

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                ADOconnection.Close();
            }

            return cubeDataSet;
        }
    }
}
