﻿using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MongoSimple2016
{
    public class MongoAccess
    {
        //readonly 
        MongoDatabase mongoDatabase;

        public void CreateContact(Contact newContact)
        {
            try
            {

                mongoDatabase = RetreiveMongohqDb();

                var contactsList = mongoDatabase.GetCollection("ContactList");
                WriteConcernResult result;
                bool hasError = false;
                if (string.IsNullOrEmpty(newContact.Id))
                {
                    newContact.Id = ObjectId.GenerateNewId().ToString();
                    result = contactsList.Insert<Contact>(newContact);
                    hasError = result.HasLastErrorMessage;
                }
                else
                {
                    IMongoQuery query = Query.EQ("_id", newContact.Id);
                    IMongoUpdate update = Update
                     .Set("Name", newContact.Name)
                     .Set("Address", newContact.Address)
                     .Set("Phone", newContact.Phone)
                     .Set("Email", newContact.Email);
                    result = contactsList.Update(query, update);
                    //hasError = result.HasLastErrorMessage;
                }
            }
            catch (Exception ex)
            {

                throw;
            }

        }

        public List<Contact> GetAllContacts()
        {
            mongoDatabase = RetreiveMongohqDb();
            List<Contact> model = new List<Contact>();
            var contactsList = mongoDatabase.GetCollection("ContactList").FindAll().AsEnumerable();
            model = (from contact in contactsList
                     select new Contact
                     {
                         Id = contact["_id"].AsString,
                         Name = contact["Name"].AsString,
                         Address = contact["Address"].AsString,
                         Phone = contact["Phone"].AsString,
                         Email = contact["Email"].AsString
                     }).ToList();
            return model;
        }


        private MongoDatabase RetreiveMongohqDb()
        {
            MongoClient mongoClient = new MongoClient(
                new MongoUrl(@"mongodb://gonk:gonk@ds157278.mlab.com:57278/2016-isit-420"));
            MongoServer server = mongoClient.GetServer();
            return mongoClient.GetServer().GetDatabase("2016-isit-420");
        }
    }
}
