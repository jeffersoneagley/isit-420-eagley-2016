﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MongoSimple2016
{
    class Program
    {
        static void Main(string[] args)
        {
            // this article helped
            // http://www.dotnetcurry.com/aspnet/897/mongodb-aspnet-webapi-connection

            MongoAccess myMongoAccess = new MongoAccess();

            Contact testContact = new Contact();
            testContact.Name = "kurt3";
            testContact.Address = "someplace3";
            testContact.Phone = "12345673";
            testContact.Email = "kurt3@there.com";

            //myMongoAccess.CreateContact(testContact);

            List<Contact> myList = myMongoAccess.GetAllContacts();

            foreach (Contact item in myList)
            {
                Console.WriteLine(item.Name + " " + item.Address);
            }

            Console.WriteLine("back to Main");
            Console.ReadLine();
        }
    }
}
