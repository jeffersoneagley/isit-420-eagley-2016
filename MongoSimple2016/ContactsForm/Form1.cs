﻿using MongoSimple2016;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ContactsForm
{
    public partial class Form1 : Form
    {
        MongoAccess myMongoAccess;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            myMongoAccess = new MongoAccess();
            UpdateView();
        }

        private void UpdateView()
        {
            listView1.Columns.Add("Name", 80);
            listView1.Columns.Add("Address", 100);
            listView1.Columns.Add("Phone", 100);
            listView1.Columns.Add("Email", 100);
            listView1.BackColor = System.Drawing.Color.LightBlue;  // just playing with the listview props and appearance
            listView1.ForeColor = System.Drawing.Color.Blue;
            listView1.BorderStyle = BorderStyle.Fixed3D;
            listView1.GridLines = true;
            listView1.View = View.Details; // else you get icons!!

            try
            {
                if (listView1.Items.Count > 0)  // should really be able to do a listViewNotes.Clear(); but it wipes out the display??
                {
                    for (int i = listView1.Items.Count - 1; i >= 0; i--)  // removes the items in the ListView, before we re-add them with updated data
                    {
                        listView1.Items[i].Remove();
                    }
                }


                List<Contact> myList = myMongoAccess.GetAllContacts();
                if (myList.Count > 0)
                {
                    Contact item;  // create a temp object of the rigth type to pull each item out of the returned List
                    for (int i = 0; i < myList.Count; i++)
                    {
                        item = myList[i];
                        listView1.Items.Add(item.Name);
                        listView1.Items[i].SubItems.Add(item.Address);
                        listView1.Items[i].SubItems.Add(item.Phone);
                        listView1.Items[i].SubItems.Add(item.Email);
                    }
                }
                else
                {
                    MessageBox.Show("No Contacts Found");
                    this.Close();
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
                this.Close();
            }

        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            Contact newContact = new Contact();
            newContact.Name = textBoxName.Text;
            newContact.Address = textBoxAddress.Text;
            newContact.Phone = textBoxPhone.Text;
            newContact.Email = textBoxEmail.Text;

            myMongoAccess.CreateContact(newContact);

            UpdateView();
        }
    }
}
