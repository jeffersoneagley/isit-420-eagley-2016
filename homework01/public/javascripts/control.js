define([], function() {
  var numberOfEntries = 450;
  var myTimeout = null;
  var itemIds = [123456, 123654, 321456, 321654, 654123,
    654321, 543216, 354126, 621453, 623451];

  var Control = function() {
    init();
  };

  function init() {
    console.log('gonk');
    myTimeout = setTimeout(function() {
      createNewDummyPurchase();
    }, Math.floor(Math.random() * 1000));
  }

  function randomDetail() {
    var details = {
      'storeNumber': 'test',
      'salesPersonID': '123',
      'itemNumber': '123',
      'timePurch': Date.now(),
      'pricePaid': '1'
    };
    var storeids = [98053, 98007, 98077, 98055, 98011, 98046];

    details.storeNumber = Math.floor(Math.random() * storeids.length);
    details.salesPersonID =
      (details.storeNumber * 4) +
      Math.floor(Math.random() * 4);
    details.itemNumber = itemIds[Math.floor(Math.random() * itemIds.length)];
    details.pricePaid = 5 + (Math.round(Math.random() * 100 * 10) / 100);

    return details;
  }

  function createNewDummyPurchase() {
    $.getJSON('/purchase', randomDetail(), function(res, status) {
      if (res.rowCount < numberOfEntries) {
        fillLog('rowcount ' + res.rowCount + ', status ' + status);
        myTimeout = window.setTimeout(function() {
          createNewDummyPurchase();
        }, 500 + Math.floor(Math.random() * 1500));
      }else {
        myTimeout = null;
      }
    });
  }

  function fillLog(item) {
    $('<p>').html(item).prependTo($('#logOutput'));

  }

  return Control;
});
