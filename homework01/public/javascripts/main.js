requirejs.config({
    baseUrl: '/',
    paths: {
        'jquery': 'bower_components/jquery/dist/jquery',
        'control': 'javascripts/control'
      }
  });

requirejs(['jquery'], function($) {
    'use strict';
    requirejs(['control'], function(Control) {
        $(document)
            .ready(function() {
                var control = new Control();
              });
      });
  });
