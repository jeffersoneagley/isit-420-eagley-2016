var express = require('express');
var router = express.Router();
var purchase = require('./purchase')();

router.use('/purchase', purchase);

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', {title: 'Dummy data filler'});
});

module.exports = router;
