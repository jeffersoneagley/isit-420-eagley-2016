module.exports = function() {
  'use strict';
  var express = require('express');
  var router = express.Router();
  var connectionString
    = 'mongodb://uploader:cheesecake@ds157278.mlab.com:57278/2016-isit-420';
  var mongodb = require('mongodb');
  var mongoclient = mongodb.MongoClient;
  var purchasedb = null;

  mongoclient.connect(connectionString, function(err, db) {
    if (err === null) {
      console.log('Connected correctly to server');
      purchasedb = db;
    }else {
      console.log('Failed to connect to server');
      throw new Error('Failed to connect to mongo');
    }
  });

  /* GET home page. */
  router.get('/', function(req, res, next) {
    var collection = purchasedb.collection('purchase');
    try {
      console.log(req.query);
      if (req.query) {
        collection.insertOne(req.query, function(err, results) {
          if (err) {
            console.log(err);
          }else {
            collection.count({}, function(err, num) {
              console.log(err);
              console.log(num);
              res.send({'rowCount': num});
            });
          }
        });

      }
    } catch (e) {
      console.log(e.message);
      collection.count({}, function(err, num) {
        res.send({'rowCount': num});
      });
    } finally {

    }
  });

  return router;
};
