﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using _420FinalEagleyKasner.Models;
using _420FinalEagleyKasner.Models.ViewModels;

namespace _420FinalEagleyKasner.Controllers
{

    public class BrowserController : Controller
    {
        isit420FinalProjectEntities db = new isit420FinalProjectEntities();
        // GET: Browser
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult FindCities(_420FinalEagleyKasner.Models.ViewModels.ViewModelCitySearch model)
        {
            float tempBorderWarm = 60;
            float tempBorderCold = 50;

            float TemperatureMin = model.TemperatureCold ? -999 : (model.TemperatureMild ? tempBorderCold : tempBorderWarm);
            float TemperatureMax = model.TemperatureWarm ? 999 : (model.TemperatureMild ? tempBorderWarm : tempBorderCold);


            var data = db.LivingCosts
                .Where(w => w.State1.Temperature.AverageTempF > TemperatureMin && w.State1.Temperature.AverageTempF < TemperatureMax).ToList();
            var viewData = data.Select(w=> new ViewModelCityResult(w));

            return View(viewData.ToList().OrderByDescending(w=>w.SalaryToCostOfLiving()).ToList());
        }

        public ActionResult AllSalaries()
        {
            return View(db.Salaries.OrderByDescending(w=>w.AnnualMeanWage).ToList());
        }
    }
}