﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _420FinalEagleyKasner.Models.ViewModels
{
    public class ViewModelCityResult
    {
        public ViewModelCityResult(LivingCost w)
        {
            livingCost = w;
        }
        public LivingCost livingCost;
        public double SalaryToCostOfLiving() {
                return livingCost.State1.Salaries.Average(
                    sal => sal.AnnualMeanWage).Value / Convert.ToDouble(livingCost.Cost_of_Living_Plus_Rent_Index
                    );
            }
    }
}