﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _420FinalEagleyKasner.Models.ViewModels
{
    public class ViewModelCitySearch
    {
        public bool TemperatureWarm { get; set; }
        public bool TemperatureCold { get; set; }
        public bool TemperatureMild { get; set; }
    }
}