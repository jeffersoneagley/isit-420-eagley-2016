//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace _420FinalEagleyKasner.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class LivingCost
    {
        public int CityID { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Cost_of_Living_Index { get; set; }
        public string Rent_Index { get; set; }
        public string Cost_of_Living_Plus_Rent_Index { get; set; }
        public string Groceries_Index { get; set; }
        public string Restaurant_Price_Index { get; set; }
        public string Local_Purchasing_Power_Index { get; set; }
    
        public virtual State State1 { get; set; }
    }
}
