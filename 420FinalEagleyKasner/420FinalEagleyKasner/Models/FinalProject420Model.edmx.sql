
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 03/18/2017 21:21:07
-- Generated from EDMX file: D:\Users\fish\OneDrive\Documents\_university\_BC\Classes\2017\winter\isit420\420FinalEagleyKasner\420FinalEagleyKasner\Models\FinalProject420Model.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [isit420FinalProject];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_cities_costOfLiving_States1]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Cities] DROP CONSTRAINT [FK_cities_costOfLiving_States1];
GO
IF OBJECT_ID(N'[dbo].[FK_Salaries_JobTypes]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Salaries] DROP CONSTRAINT [FK_Salaries_JobTypes];
GO
IF OBJECT_ID(N'[dbo].[FK_Salaries_States]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Salaries] DROP CONSTRAINT [FK_Salaries_States];
GO
IF OBJECT_ID(N'[dbo].[FK_Temperatures_States]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Temperatures] DROP CONSTRAINT [FK_Temperatures_States];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Cities]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Cities];
GO
IF OBJECT_ID(N'[dbo].[JobTypes]', 'U') IS NOT NULL
    DROP TABLE [dbo].[JobTypes];
GO
IF OBJECT_ID(N'[dbo].[Salaries]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Salaries];
GO
IF OBJECT_ID(N'[dbo].[States]', 'U') IS NOT NULL
    DROP TABLE [dbo].[States];
GO
IF OBJECT_ID(N'[dbo].[Temperatures]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Temperatures];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Cities'
CREATE TABLE [dbo].[Cities] (
    [CityID] int IDENTITY(1,1) NOT NULL,
    [City1] nvarchar(50)  NOT NULL,
    [State] char(2)  NOT NULL,
    [Cost_of_Living_Index] nvarchar(50)  NOT NULL,
    [Rent_Index] nvarchar(50)  NOT NULL,
    [Cost_of_Living_Plus_Rent_Index] nvarchar(50)  NOT NULL,
    [Groceries_Index] nvarchar(50)  NOT NULL,
    [Restaurant_Price_Index] nvarchar(50)  NOT NULL,
    [Local_Purchasing_Power_Index] nvarchar(50)  NOT NULL
);
GO

-- Creating table 'JobTypes'
CREATE TABLE [dbo].[JobTypes] (
    [JobTypeID] int IDENTITY(1,1) NOT NULL,
    [Job_Title] nvarchar(50)  NOT NULL
);
GO

-- Creating table 'Salaries'
CREATE TABLE [dbo].[Salaries] (
    [SalaryID] int IDENTITY(1,1) NOT NULL,
    [JobTypeID] int  NULL,
    [StateID] char(2)  NOT NULL,
    [Employment] float  NULL,
    [EmploymentPercentRelativeStandardError] float  NULL,
    [HourlyMeanWage] float  NULL,
    [AnnualMeanWage] float  NULL,
    [WagePercent_relative_standard_error] float  NULL,
    [Hourly10thPercentileWage] float  NULL,
    [Hourly25thPercentileWage] float  NULL,
    [HourlyMedianWage] float  NULL,
    [Hourly75thPercentileWage] float  NULL,
    [Hourly90thPercentileWage] float  NULL,
    [Annual10thPercentileWage] float  NULL,
    [Annual25thPercentileWage] float  NULL,
    [AnnualMedianWage] float  NULL,
    [Annual75thpercentileWage] float  NULL,
    [Annual90thPercentile] float  NULL,
    [EmploymentPer1000jobs] float  NULL,
    [Location_Quotient] float  NULL
);
GO

-- Creating table 'States'
CREATE TABLE [dbo].[States] (
    [StateID] char(2)  NOT NULL,
    [StateName] nvarchar(50)  NOT NULL
);
GO

-- Creating table 'Temperatures'
CREATE TABLE [dbo].[Temperatures] (
    [StateID] char(2)  NOT NULL,
    [AverageTempF] float  NOT NULL,
    [AverageTemperatureC] float  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [CityID] in table 'Cities'
ALTER TABLE [dbo].[Cities]
ADD CONSTRAINT [PK_Cities]
    PRIMARY KEY CLUSTERED ([CityID] ASC);
GO

-- Creating primary key on [JobTypeID] in table 'JobTypes'
ALTER TABLE [dbo].[JobTypes]
ADD CONSTRAINT [PK_JobTypes]
    PRIMARY KEY CLUSTERED ([JobTypeID] ASC);
GO

-- Creating primary key on [SalaryID] in table 'Salaries'
ALTER TABLE [dbo].[Salaries]
ADD CONSTRAINT [PK_Salaries]
    PRIMARY KEY CLUSTERED ([SalaryID] ASC);
GO

-- Creating primary key on [StateID] in table 'States'
ALTER TABLE [dbo].[States]
ADD CONSTRAINT [PK_States]
    PRIMARY KEY CLUSTERED ([StateID] ASC);
GO

-- Creating primary key on [StateID] in table 'Temperatures'
ALTER TABLE [dbo].[Temperatures]
ADD CONSTRAINT [PK_Temperatures]
    PRIMARY KEY CLUSTERED ([StateID] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [State] in table 'Cities'
ALTER TABLE [dbo].[Cities]
ADD CONSTRAINT [FK_cities_costOfLiving_States1]
    FOREIGN KEY ([State])
    REFERENCES [dbo].[States]
        ([StateID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_cities_costOfLiving_States1'
CREATE INDEX [IX_FK_cities_costOfLiving_States1]
ON [dbo].[Cities]
    ([State]);
GO

-- Creating foreign key on [JobTypeID] in table 'Salaries'
ALTER TABLE [dbo].[Salaries]
ADD CONSTRAINT [FK_Salaries_JobTypes]
    FOREIGN KEY ([JobTypeID])
    REFERENCES [dbo].[JobTypes]
        ([JobTypeID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Salaries_JobTypes'
CREATE INDEX [IX_FK_Salaries_JobTypes]
ON [dbo].[Salaries]
    ([JobTypeID]);
GO

-- Creating foreign key on [StateID] in table 'Salaries'
ALTER TABLE [dbo].[Salaries]
ADD CONSTRAINT [FK_Salaries_States]
    FOREIGN KEY ([StateID])
    REFERENCES [dbo].[States]
        ([StateID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Salaries_States'
CREATE INDEX [IX_FK_Salaries_States]
ON [dbo].[Salaries]
    ([StateID]);
GO

-- Creating foreign key on [StateID] in table 'Temperatures'
ALTER TABLE [dbo].[Temperatures]
ADD CONSTRAINT [FK_Temperatures_States]
    FOREIGN KEY ([StateID])
    REFERENCES [dbo].[States]
        ([StateID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------