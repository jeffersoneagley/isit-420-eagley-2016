﻿using System.Web;
using System.Web.Mvc;

namespace _420FinalEagleyKasner
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
